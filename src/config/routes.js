import Home from '../components/views/Home';

export default [
  {
    path: '/',
    name: 'home',
    exact: true,
    component: Home
  }
];
