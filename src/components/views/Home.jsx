import React, {Fragment, useState, useEffect, useRef} from 'react';
import {connect} from 'react-redux';
import Spinner from 'react-activity/lib/Spinner';
import ReactPaginate from 'react-paginate';
import {fetchProducts} from '../../actions/rootActions';
import {
  StyledPageHead,
  StyledPageDescription,
  StyledPageTitle,
  StyledPageImage,
  StyledPageContent,
  StyledSearchContainer,
  StyledSearchForm,
  StyledSearchInput,
  StyledSearchSubmit,
  StyledSearchResult,
  StyledSearchProduct,
  StyledTotalResults
} from '../../theme/styles';
import 'react-activity/lib/Spinner/Spinner.css';

const mapStateToProps = (state) => {
  return {
    products: state.root.products,
    isFetchProductsPending: state.root.isFetchProductsPending,
    fetchProductsError: state.root.fetchProductsError
  };
};

const mapDispatchToProps = {
  fetchProducts
};

const Home = (props) => {
  const perPage = 10;
  const searchResultsRef = useRef(null);
  const [offset, setOffset] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const sliceProducts = props.products.slice(offset, offset + perPage);

  useEffect(() => {
    setPageCount(Math.ceil(props.products.length / perPage));
  }, [props.products]);

  const handleSearchSubmit = async (event) => {
    event.persist();
    event.preventDefault();

    return props.fetchProducts(process.env.REACT_APP_API + '/product', {
      q: event.target[0].value,
      limit: perPage,
      page: 1
    }, searchResultsRef);
  };

  const handlePageChange = (event) => {
    let selectedPage = event.selected;
    let offset = selectedPage * perPage;

    setOffset(offset);
  };

  return (
    <Fragment>
      <StyledPageHead>
        <StyledPageDescription>
          <StyledPageTitle>This is a page for <br/> beauty product search</StyledPageTitle>
        </StyledPageDescription>

        <StyledPageImage className="page-image"></StyledPageImage>
      </StyledPageHead>

      <StyledPageContent>
        <StyledSearchContainer>
          <StyledSearchForm onSubmit={handleSearchSubmit} data-testid="form">
            <StyledSearchInput type="search" placeholder="Enter a product name or an ingredient..." aria-label="search"/>
            <StyledSearchSubmit type="submit" value="Search"/>
          </StyledSearchForm>

          {props.isFetchProductsPending ?
            <Spinner color="#55D7FF"/> :
            props.fetchProductsError !== null ?
            <StyledTotalResults><strong>Total results :</strong> {props.products.length}</StyledTotalResults> :
            sliceProducts.length > 0 ?
            <Fragment>
              <StyledSearchResult ref={searchResultsRef}>
                {sliceProducts.map((product, index) => {
                  return <StyledSearchProduct key={index}>
                    <p><strong>{product.brand.charAt(0).toUpperCase() + product.brand.slice(1)}</strong> - <span>{product.name.charAt(0).toUpperCase() + product.name.slice(1)}</span></p>
                  </StyledSearchProduct>
                })}
              </StyledSearchResult>

              <StyledTotalResults><strong>Total results :</strong> {props.products.length}</StyledTotalResults>

              <ReactPaginate
                initialPage={0}
                previousLabel={null}
                nextLabel={null}
                breakLabel="..."
                breakClassName="break-me"
                pageCount={pageCount}
                marginPagesDisplayed={2}
                pageRangeDisplayed={3}
                onPageChange={handlePageChange}
                containerClassName="pagination"
                subContainerClassName="pages pagination"
                activeClassName="active"
                previousLinkClassName="icon icon-circle-left"
                nextLinkClassName="icon icon-circle-right"/>
            </Fragment> : null}
        </StyledSearchContainer>
      </StyledPageContent>
    </Fragment>
  )
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
