import React from 'react';
import {StyledErrorPageContainer} from '../../theme/styles';

const Error = () => {
  return (
    <StyledErrorPageContainer>
      <h1>404 error page</h1>
      <p>Oops, the page you requested doesn't exist.</p>
    </StyledErrorPageContainer>
  );
};

export default Error;
