import React from 'react';
import Router from './router/Router';
import ReduxToastr from 'react-redux-toastr';

const Layout = () => {
  return (
    <main>
      <Router/>
      <ReduxToastr
        position="top-right"
        transitionIn="fadeIn"
        transitionOut="fadeOut"
        timeOut={2000}/>
    </main>
  );
};

export default Layout;
