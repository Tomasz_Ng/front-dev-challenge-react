import React from 'react';
import {Route, Switch} from 'react-router-dom';
import {connect} from 'react-redux';
import Error from '../views/Error';

const mapStateToProps = (state) => {
  return {
    routes: state.root.routes
  };
};

const Router = (props) => {
  return (
    <Switch>
      {props.routes.map((route, i) => (
        <Route
          key={i}
          path={route.path}
          exact={route.exact}
          state={route.state}
          children={routeProps => (
            <route.component {...routeProps}/>
          )}
        />
      ))}
      <Route
        children={(routeProps) => (
          <Error {...routeProps}/>
        )}
      />
    </Switch>
  );
};

export default connect(mapStateToProps, null)(Router);
