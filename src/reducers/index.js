import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router';
import {reducer as toastrReducer} from 'react-redux-toastr';
import {rootReducer, initialState as rootState} from './rootReducer';

export const initialState = Object.assign(rootState);

export const reducers = (browserHistory) => combineReducers({
    'root': rootReducer,
    'router': connectRouter(browserHistory),
    'toastr': toastrReducer
});
