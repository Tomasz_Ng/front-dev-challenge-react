import routes from '../config/routes';

export const initialState = {
  root: {
    routes: routes,
    products: [],
    isFetchProductsPending: false,
    fetchProductsError: null
  }
};

export const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'FETCH_PRODUCTS_PENDING':
      return {
        ...state,
        isFetchProductsPending: true,
      };
    case 'FETCH_PRODUCTS_SUCCESS':
      return {
        ...state,
        isFetchProductsPending: false,
        products: action.response,
        fetchProductsError: null
      };
    case 'FETCH_PRODUCTS_ERROR':
      return {
        ...state,
        isFetchProductsPending: false,
        fetchProductsError: action.error,
        products: []
      };
    default:
      return state;
  }
};
