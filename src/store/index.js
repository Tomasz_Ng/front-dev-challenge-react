import {applyMiddleware, compose, createStore} from 'redux';
import {routerMiddleware} from 'connected-react-router';
import {createBrowserHistory} from 'history';
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';
import {reducers, initialState} from '../reducers';

export const browserHistory = createBrowserHistory();

const middlewares = [thunk, routerMiddleware(browserHistory)];
const enhancer = process.env.NODE_ENV === 'development' ? composeWithDevTools : compose;

export const store = createStore(
  reducers(browserHistory),
  initialState,
  enhancer(
    applyMiddleware(
      ...middlewares
    )
  )
);
