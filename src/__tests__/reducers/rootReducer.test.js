import {rootReducer} from '../../reducers/rootReducer';
import * as types from '../../constants/action-types';

describe('root reducer', () => {
  it('should return the initial state', () => {
    expect(rootReducer(undefined, {})).toMatchSnapshot();
  });

  it('should handle FETCH_PRODUCTS_PENDING', () => {
    expect(
      rootReducer([], {
        type: types.FETCH_PRODUCTS_PENDING
      })
    ).toMatchSnapshot();
  });

  it('should handle FETCH_PRODUCTS_SUCCESS', () => {
    expect(
      rootReducer([], {
        type: types.FETCH_PRODUCTS_SUCCESS,
        response: []
      })
    ).toMatchSnapshot();
  });

  it('should handle FETCH_PRODUCTS_ERROR', () => {
    expect(
      rootReducer([], {
        type: types.FETCH_PRODUCTS_ERROR,
        error: ''
      })
    ).toMatchSnapshot();
  });
});
