import React from 'react';
import {render} from '@testing-library/react'
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import {Router as RouterDom} from 'react-router-dom';
import {createMemoryHistory} from 'history';
import Router from '../../../components/router/Router';
import {store} from '../../../store';

const mockStore = configureStore();

describe('Router', () => {
  let history;
  let initialState;

  beforeEach(() => {
    initialState = mockStore(store.getState());
    history = createMemoryHistory();
  }, 0);

  it('should land on a home page', () => {
    history.push('/');

    let {container} = render(
      <Provider store={initialState}>
        <RouterDom history={history}>
          <Router />
        </RouterDom>
      </Provider>);

    expect(container).toMatchSnapshot();
  });

  it('should land on a 404 page', () => {
    history.push('/error');

    let {container} = render(
      <Provider store={initialState}>
        <RouterDom history={history}>
          <Router />
        </RouterDom>
      </Provider>);

    expect(container).toMatchSnapshot();
  })
});
