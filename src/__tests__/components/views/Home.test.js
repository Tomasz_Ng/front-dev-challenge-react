import React from 'react';
import {render, fireEvent} from '@testing-library/react';
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import Home from '../../../components/views/Home';
import {store} from '../../../store';

const mockStore = configureStore();

describe('Home', () => {
  let initialState;

  it('should render properly', () => {
    initialState = mockStore(store.getState());

    let {container} = render(
      <Provider store={initialState}>
        <Home />
      </Provider>);

    expect(container).toMatchSnapshot();
  });

  it('should fetch products on form submit and return results with pagination', async () => {
    let teststore = store.getState();
    teststore.root.products = [
      {
        brand: 'brand1',
        name: 'name1'
      },
      {
        brand: 'brand2',
        name: 'name2'
      }
    ];
    initialState = mockStore(teststore);

    let {getByTestId, container} = render(
      <Provider store={initialState}>
        <Home />
      </Provider>);

    fireEvent.submit(getByTestId('form'), {target: [{value: 'value'}]});

    expect(container).toMatchSnapshot();
  });

  it('should show spinner when fetch products pending', async () => {
    let teststore = store.getState();
    teststore.root.isFetchProductsPending = true;
    initialState = mockStore(teststore);

    let {container} = render(
      <Provider store={initialState}>
        <Home />
      </Provider>);

    expect(container).toMatchSnapshot();
  });

  it('should show no results when fetch products fails', async () => {
    let teststore = store.getState();
    teststore.root.isFetchProductsPending = false;
    teststore.root.fetchProductsError = 'error';
    initialState = mockStore(teststore);

    let {container} = render(
      <Provider store={initialState}>
        <Home />
      </Provider>);

    expect(container).toMatchSnapshot();
  });
});
