import React from 'react';
import {render} from '@testing-library/react'
import Error from '../../../components/views/Error';

describe('Error', () => {
  it('should render properly', () => {
    let {container} = render(<Error />);
    expect(container).toMatchSnapshot();
  });
});
