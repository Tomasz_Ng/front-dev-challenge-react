import React from 'react';
import ReactDOM from 'react-dom';
import App from '../App';

describe('App', () => {
  it('should render without crashing', () => {
    let root = document.createElement('div');
    ReactDOM.render(<App />, root);
  });
});
