import {store} from '../../store';
import * as actions from '../../actions/rootActions';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
const withQuery = require('with-query').default;

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('rootActions', () => {
  let initialState;
  let query = {
    q: '',
    limit: 10,
    page: 1
  };
  let url = withQuery('/product', query);

  beforeEach(() => {
    initialState = mockStore(store.getState());
  }, 0);

  afterEach(() => {
    fetchMock.restore();
  }, 0);

  it('should create an action to fetch products successfully', () => {
    let ref = {
      current: {
        scrollIntoView: jest.fn()
      }
    };

    fetchMock.mock(url, {
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'GET'
    });

    return initialState.dispatch(actions.fetchProducts(url, query, ref))
      .then(() => {
        let actions = initialState.getActions();
        expect(actions[0]).toMatchSnapshot();
        expect(actions[1]).toMatchSnapshot();
      });
  });

  it('should create an action to fail fetching products', () => {
    let ref = jest.fn();

    return initialState.dispatch(actions.fetchProducts(url, query, ref))
      .then(() => {
        let actions = initialState.getActions();
        expect(actions[0]).toMatchSnapshot();
        expect(actions[1]).toMatchSnapshot();
      });
  });
});
