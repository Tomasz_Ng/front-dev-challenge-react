import {createGlobalStyle} from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'Roboto-Light';
    font-weight: normal;
    font-style: normal;
    font-display: swap;
    unicode-range: U+000-5FF;
    src: local('Roboto-Light'),
    url('/fonts/Roboto-Light.ttf') format('truetype');
  }
  
  @font-face {
    font-family: 'Roboto-Regular';
    font-weight: normal;
    font-style: normal;
    font-display: swap;
    unicode-range: U+000-5FF;
    src: local('Roboto-Regular'),
    url('/fonts/Roboto-Regular.ttf') format('truetype');
  }
  
  @font-face {
    font-family: 'Icons';
    src:  url('/fonts/Icons.ttf') format('truetype');
    font-weight: normal;
    font-style: normal;
    font-display: block;
  }
  
  * {
    margin: 0;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    -webkit-text-stroke: 0.45px rgba(0, 0, 0, 0.1);
  }
  
  body {
    font-family: 'Roboto-Light', sans-serif;
  }
  
  main {
    display: flex;
    flex-direction: column;
  }
  
  .icon {
    font-family: 'Icons', sans-serif !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
  }
  
  .icon-circle-right:before {
    content: "\\ea42";
  }
  
  .icon-circle-left:before {
    content: "\\ea44";
  }
  
  .redux-toastr {
    .top-right {
      top: 50px;
    }

    .error-toastr {
      background-color: #DB0992;
    }
  }
  
  @keyframes rai-spinner {
    from {
      transform: rotate(0deg); 
    } to {
      transform: rotate(360deg); 
    } 
  }
  
  .rai-activity-indicator {
    display: flex !important;
    justify-content: center;
    align-items: center;
    margin-top: 25px;
    margin-bottom: 25px;
  }
  
  .rai-spinner {
    width: 1.5em;
    height: 1.5em;
    position: relative;
    margin: 0; 
  }
  
  .rai-spinner-inner, .rai-spinner-outer {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    border-radius: 100%;
    margin-left: -0.0625em;
    margin-top: -0.0625em; 
  }
  
  .rai-spinner-outer {
    border: 0.125em solid #727981;
    opacity: 0.2;
  }
  
  .rai-spinner-inner {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    border-radius: 100%;
    border-top: 0.125em solid #727981;
    border-right: 0.125em solid transparent !important;
    border-bottom: 0.125em solid transparent !important;
    border-left: 0.125em solid transparent !important;
    animation-name: rai-spinner;
    animation-iteration-count: infinite;
    animation-timing-function: linear;
    animation-duration: inherit;
  }
  
  .pagination {
    padding: 0;
    list-style-type: none;
    display: flex;
    height: 40px;
    align-items: center;
    justify-content: center;

    li {
      flex: 1;
      display: flex;
      align-items: center;
      justify-content: center;
      
      a {
        width: 30px;
        height: 30px;
        border-radius: 50%;
        border: 2px solid white;
        color: white;
        display: flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
        
        &:hover {
          background-color: #55D7FF;
          color: black;
        }
        
        &:focus,
        &:active {
          outline: none;
        }
      }
      
      &.previous,
      &.next {
        a {
          border: none;
          font-size: 25px;
          
          &:hover {
            background-color: transparent;
            color: white;
          }
        }
      }
    }
    
    li.active a {
      background-color: #55D7FF;
      color: black;
    }
  }
`;
