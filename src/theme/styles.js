import styled from 'styled-components';

export const StyledPageHead = styled.section`
  display: flex;
  height: 100vh;
  flex-direction: column;
  @media screen and (min-width: 768px) {
    flex-direction: row;
    height: calc((100vh / 3) * 2);
  }
`;

export const StyledPageDescription = styled.article`
  flex: 1;
  justify-content: center;
  align-items: center;
  display: flex;
  background-color: #55D7FF;
`;

export const StyledPageTitle = styled.h1`
  font-family: 'Roboto-Regular', sans-serif;
  font-weight: bold;
  font-size: 4vh;
  text-align: center;
`;

export const StyledPageImage = styled.aside`
  flex: 1;
  background-image: url('/images/beauty-products.jpg');
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
`;

export const StyledPageContent = styled.section`
  display: flex;
  height: 100%;
  min-height: calc(100vh / 3);
  justify-content: center;
  background-color: #DB0992;
`;

export const StyledSearchContainer = styled.aside`
  width: 100%;
  padding: 5vw 15px 5vw 15px;
  @media screen and (min-width: 768px) {
    padding: 4vw 15px 4vw 15px;
    width: 50%;
  }
`;

export const StyledSearchForm = styled.form`
  height: 40px;
  display: flex;
  justify-content: center;
`;

export const StyledSearchInput = styled.input`
  height: 100%;
  width: 100%;
  margin-right: 15px;
  border-radius: 5px;
  border: none;
  padding-left: 15px;
  padding-right: 15px;
  font-size: 16px;

  &:focus,
  &:active {
    outline: none;
  }
`;

export const StyledSearchSubmit = styled.input`
  height: 100%;
  width: 100px;
  border-radius: 5px;
  border: none;
  background-color: #55D7FF;
  cursor: pointer;
  font-size: 16px;

  &:focus,
  &:active {
    outline: none;
  }
`;

export const StyledSearchResult = styled.ul`
  margin-top: 25px;
  padding-left: 20px;
`;

export const StyledSearchProduct = styled.li`
  color: white;
  font-size: 22px;
`;

export const StyledTotalResults = styled.p`
  text-align: center;
  margin-top: 25px;
  margin-bottom: 25px;
  color: white;
`;

export const StyledErrorPageContainer = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100vh;
`;


