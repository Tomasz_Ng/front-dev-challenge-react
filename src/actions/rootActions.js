import {
  FETCH_PRODUCTS_PENDING,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_ERROR,
} from '../constants/action-types';
import {toastr} from 'react-redux-toastr';

const withQuery = require('with-query').default;

// Thunks
function fetchProductsPending() {
  return {
    type: FETCH_PRODUCTS_PENDING
  }
}

function fetchProductsSuccess(response) {
  return {
    type: FETCH_PRODUCTS_SUCCESS,
    response: response
  }
}

function fetchProductsError(error) {
  return {
    type: FETCH_PRODUCTS_ERROR,
    error: error
  }
}

// Exports
export const fetchProducts = (url, query, searchResultsRef) => {
  return (dispatch) => {
    dispatch(fetchProductsPending());

    return fetch(withQuery(url, query), {
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'GET'
    })
    .then(response => response.json())
    .then(response => {
      dispatch(fetchProductsSuccess(response));
      searchResultsRef.current.scrollIntoView({behavior: 'smooth'});
    })
    .catch(error => {
      dispatch(fetchProductsError(error));
      toastr.error('An error occured', 'No results found', {className: 'error-toastr'});
    });
  }
};
