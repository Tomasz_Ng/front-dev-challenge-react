import React from 'react';
import {ConnectedRouter} from 'connected-react-router';
import {Provider} from 'react-redux';
import {browserHistory, store} from './store';
import Layout from './components/Layout';
import {GlobalStyle} from './theme/globalStyles';
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css';

const App = () => {
  return (
    <Provider store={store}>
      <ConnectedRouter history={browserHistory}>
        <GlobalStyle/>
        <Layout/>
      </ConnectedRouter>
    </Provider>
  );
};

export default App;
